﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HelpDesk.DataContract
{
    public class TicketDto : DtoBase
    {
        public string Header { get; set; }
        public string Description { get; set; }
        public Guid AuthorId { get; set; }
        //public virtual IList<User> CoExecutors { get; set; }
        //public Statuses Status { get; set; }
        //public Priorities Priority { get; set; }
        //public Classifiers Classifier { get; set; }


    }
}
