﻿using System;

namespace HelpDesk.DataContract
{
    public abstract class DtoBase
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}