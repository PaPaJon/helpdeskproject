﻿using System;

namespace HelpDesk.DataContract
{
    public class CommentDto : DtoBase
    {
        public Guid TicketId { get; set; }
        public DateTime DateCreated { get; set; }
        public UserDto CreatedByUser { get; set; }
        public string Text { get; set; }
    }
}
