﻿namespace HelpDesk.WinFormsClient
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btDelete = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.правкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сервисToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.опрогаммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbDateForChangeResult = new System.Windows.Forms.Label();
            this.lbCoExecutorsResult = new System.Windows.Forms.Label();
            this.lbAutorResult = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbDataForChange = new System.Windows.Forms.Label();
            this.lbCoExecutors = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbComments = new System.Windows.Forms.Label();
            this.lbHeader = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbAutor = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbDateForCreateResult = new System.Windows.Forms.Label();
            this.lbDeadLineResult = new System.Windows.Forms.Label();
            this.lbDataForCreate = new System.Windows.Forms.Label();
            this.lbDeadLine = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btDelete);
            this.groupBox1.Controls.Add(this.btAdd);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(182, 398);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Список заявок";
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(89, 342);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 23);
            this.btDelete.TabIndex = 2;
            this.btDelete.Text = "Удалить";
            this.btDelete.UseVisualStyleBackColor = true;
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(7, 342);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(75, 23);
            this.btAdd.TabIndex = 1;
            this.btAdd.Text = "Создать";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.BtAdd_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(170, 316);
            this.listBox1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.правкаToolStripMenuItem,
            this.сервисToolStripMenuItem,
            this.опрогаммеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(797, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // правкаToolStripMenuItem
            // 
            this.правкаToolStripMenuItem.Name = "правкаToolStripMenuItem";
            this.правкаToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.правкаToolStripMenuItem.Text = "Правка";
            // 
            // сервисToolStripMenuItem
            // 
            this.сервисToolStripMenuItem.Name = "сервисToolStripMenuItem";
            this.сервисToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.сервисToolStripMenuItem.Text = "Сервис";
            // 
            // опрогаммеToolStripMenuItem
            // 
            this.опрогаммеToolStripMenuItem.Name = "опрогаммеToolStripMenuItem";
            this.опрогаммеToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.опрогаммеToolStripMenuItem.Text = "Опрогамме";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.richTextBox2);
            this.groupBox2.Controls.Add(this.richTextBox1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lbDateForChangeResult);
            this.groupBox2.Controls.Add(this.lbCoExecutorsResult);
            this.groupBox2.Controls.Add(this.lbAutorResult);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lbDataForChange);
            this.groupBox2.Controls.Add(this.lbCoExecutors);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lbComments);
            this.groupBox2.Controls.Add(this.lbHeader);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lbAutor);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lbDateForCreateResult);
            this.groupBox2.Controls.Add(this.lbDeadLineResult);
            this.groupBox2.Controls.Add(this.lbDataForCreate);
            this.groupBox2.Controls.Add(this.lbDeadLine);
            this.groupBox2.Location = new System.Drawing.Point(200, 40);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(588, 398);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Заявка";
            this.groupBox2.Enter += new System.EventHandler(this.GroupBox2_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox3);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(10, 238);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(305, 154);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Написать комментарий";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(6, 19);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(293, 54);
            this.richTextBox3.TabIndex = 1;
            this.richTextBox3.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 125);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(321, 45);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(261, 347);
            this.richTextBox2.TabIndex = 1;
            this.richTextBox2.Text = "";
            this.richTextBox2.TextChanged += new System.EventHandler(this.RichTextBox2_TextChanged);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(10, 163);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(305, 56);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(102, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "null";
            // 
            // lbDateForChangeResult
            // 
            this.lbDateForChangeResult.AutoSize = true;
            this.lbDateForChangeResult.Location = new System.Drawing.Point(102, 45);
            this.lbDateForChangeResult.Name = "lbDateForChangeResult";
            this.lbDateForChangeResult.Size = new System.Drawing.Size(23, 13);
            this.lbDateForChangeResult.TabIndex = 0;
            this.lbDateForChangeResult.Text = "null";
            // 
            // lbCoExecutorsResult
            // 
            this.lbCoExecutorsResult.AutoSize = true;
            this.lbCoExecutorsResult.Location = new System.Drawing.Point(102, 71);
            this.lbCoExecutorsResult.Name = "lbCoExecutorsResult";
            this.lbCoExecutorsResult.Size = new System.Drawing.Size(23, 13);
            this.lbCoExecutorsResult.TabIndex = 0;
            this.lbCoExecutorsResult.Text = "null";
            // 
            // lbAutorResult
            // 
            this.lbAutorResult.AutoSize = true;
            this.lbAutorResult.Location = new System.Drawing.Point(102, 19);
            this.lbAutorResult.Name = "lbAutorResult";
            this.lbAutorResult.Size = new System.Drawing.Size(23, 13);
            this.lbAutorResult.TabIndex = 0;
            this.lbAutorResult.Text = "null";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Приоритет :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(102, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "null";
            // 
            // lbDataForChange
            // 
            this.lbDataForChange.AutoSize = true;
            this.lbDataForChange.Location = new System.Drawing.Point(7, 45);
            this.lbDataForChange.Name = "lbDataForChange";
            this.lbDataForChange.Size = new System.Drawing.Size(98, 13);
            this.lbDataForChange.TabIndex = 0;
            this.lbDataForChange.Text = "Дата изменения :";
            // 
            // lbCoExecutors
            // 
            this.lbCoExecutors.AutoSize = true;
            this.lbCoExecutors.Location = new System.Drawing.Point(7, 71);
            this.lbCoExecutors.Name = "lbCoExecutors";
            this.lbCoExecutors.Size = new System.Drawing.Size(61, 13);
            this.lbCoExecutors.TabIndex = 0;
            this.lbCoExecutors.Text = "Участники";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "null";
            // 
            // lbComments
            // 
            this.lbComments.AutoSize = true;
            this.lbComments.Location = new System.Drawing.Point(369, 16);
            this.lbComments.Name = "lbComments";
            this.lbComments.Size = new System.Drawing.Size(131, 13);
            this.lbComments.TabIndex = 0;
            this.lbComments.Text = "Комментари учатсников";
            // 
            // lbHeader
            // 
            this.lbHeader.AutoSize = true;
            this.lbHeader.Location = new System.Drawing.Point(7, 147);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(23, 13);
            this.lbHeader.TabIndex = 0;
            this.lbHeader.Text = "null";
            this.lbHeader.Click += new System.EventHandler(this.LbDescription_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Классификация :";
            // 
            // lbAutor
            // 
            this.lbAutor.AutoSize = true;
            this.lbAutor.Location = new System.Drawing.Point(7, 19);
            this.lbAutor.Name = "lbAutor";
            this.lbAutor.Size = new System.Drawing.Size(43, 13);
            this.lbAutor.TabIndex = 0;
            this.lbAutor.Text = "Автор :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Статус :";
            // 
            // lbDateForCreateResult
            // 
            this.lbDateForCreateResult.AutoSize = true;
            this.lbDateForCreateResult.Location = new System.Drawing.Point(102, 32);
            this.lbDateForCreateResult.Name = "lbDateForCreateResult";
            this.lbDateForCreateResult.Size = new System.Drawing.Size(23, 13);
            this.lbDateForCreateResult.TabIndex = 0;
            this.lbDateForCreateResult.Text = "null";
            // 
            // lbDeadLineResult
            // 
            this.lbDeadLineResult.AutoSize = true;
            this.lbDeadLineResult.Location = new System.Drawing.Point(102, 58);
            this.lbDeadLineResult.Name = "lbDeadLineResult";
            this.lbDeadLineResult.Size = new System.Drawing.Size(23, 13);
            this.lbDeadLineResult.TabIndex = 0;
            this.lbDeadLineResult.Text = "null";
            // 
            // lbDataForCreate
            // 
            this.lbDataForCreate.AutoSize = true;
            this.lbDataForCreate.Location = new System.Drawing.Point(7, 32);
            this.lbDataForCreate.Name = "lbDataForCreate";
            this.lbDataForCreate.Size = new System.Drawing.Size(90, 13);
            this.lbDataForCreate.TabIndex = 0;
            this.lbDataForCreate.Text = "Дата создания :";
            // 
            // lbDeadLine
            // 
            this.lbDeadLine.AutoSize = true;
            this.lbDeadLine.Location = new System.Drawing.Point(7, 58);
            this.lbDeadLine.Name = "lbDeadLine";
            this.lbDeadLine.Size = new System.Drawing.Size(59, 13);
            this.lbDeadLine.TabIndex = 0;
            this.lbDeadLine.Text = "DeadLine :";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(229, 439);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 547);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem правкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сервисToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem опрогаммеToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label lbDeadLine;
        private System.Windows.Forms.Label lbAutor;
        private System.Windows.Forms.Label lbAutorResult;
        private System.Windows.Forms.Label lbDeadLineResult;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbCoExecutorsResult;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbCoExecutors;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbComments;
        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Label lbDateForChangeResult;
        private System.Windows.Forms.Label lbDataForChange;
        private System.Windows.Forms.Label lbDateForCreateResult;
        private System.Windows.Forms.Label lbDataForCreate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

