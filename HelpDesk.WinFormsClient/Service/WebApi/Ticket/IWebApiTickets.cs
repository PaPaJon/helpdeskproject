﻿using HelpDesk.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.DataContract;

namespace HelpDesk.WinFormsClient.Service.WebApi.Ticket
{
    public interface IWebApiTickets
    {
        void Add(TicketDto ticket);
        IList<TicketDto> Get(TicketDto ticketDto);
        TicketDto Get(Guid guid);
        TicketDto Update(TicketDto ticket);
        void Delete(Guid guid);
    }
}
