﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.DataContract;
using HelpDesk.DAL.Repositories.Interfaces;
using Newtonsoft.Json;

namespace HelpDesk.WinFormsClient.Service.WebApi.Ticket
{
    class WebApiTickets : IWebApiTickets
    {
        public void Add(TicketDto ticket)
        {
            HttpClient client = new HttpClient();
            string ticketJsString = JsonConvert.SerializeObject(ticket);
            var httpContent = new StringContent(ticketJsString, Encoding.UTF8, "application/json");
            var response = client.PostAsync("http://localhost:51939/api/ticket/", httpContent).Result;
        }

        public TicketDto Update(TicketDto ticket)
        {
            HttpClient client = new HttpClient();
            string ticketJsString = JsonConvert.SerializeObject(ticket);
            var httpContent = new StringContent(ticketJsString, Encoding.UTF8, "application/json");
            var response = client.PutAsync("http://localhost:51939/api/ticket/", httpContent).Result;
            return new TicketDto();
        }

        public void Delete(Guid guid)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responce = client.DeleteAsync("http://localhost:51939/api/ticket/" + guid).Result;
        }

        public IList<TicketDto> Get(TicketDto ticketDto)
        {
            HttpClient client = new HttpClient();
            string ticketJsString = JsonConvert.SerializeObject(ticketDto);
            var httpContent = new StringContent(ticketJsString, Encoding.UTF8, "application/json");
            var response = client.PutAsync("http://localhost:51939/api/ticket/", httpContent).Result;

            var jsonsCurrencies = response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<IList<TicketDto>>(jsonsCurrencies);
            return result;

            //HttpResponseMessage responce = client.GetAsync("http://localhost:51939/api/ticket/").Result;
            //var jsonsCurrencies = responce.Content.ReadAsStringAsync().Result;
            //return JsonConvert.DeserializeObject<List<Category>>(jsonsCurrencies);
        }

        public TicketDto Get(Guid guid)
        {
            return new TicketDto();
        }
    }
}
