﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Utility
{
    static class GuidGenerator
    {
        public static Guid GetGuid()
        {
            return Guid.NewGuid();
        }
    }
}
