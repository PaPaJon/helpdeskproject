﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HelpDesk.Api.Entity;
using HelpDesk.DataContract;

namespace HelpDesk.AutoMapper
{
    public  class AutoMapperConfiguration : MapperConfiguration
    {
        public AutoMapperConfiguration() : base(cfg =>
        {
            cfg.CreateMap<Ticket, TicketDto>();
            cfg.CreateMap<TicketDto, Ticket>();

            //lots more maps...?)
        }){}
    }

   
}
