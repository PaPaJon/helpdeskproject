﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace HelpDesk.AutoMapper
{
    public static class  AutoMapperBuilder
    {
        public static IMapper GetMapper<TCOnfig>()
        where  TCOnfig : MapperConfiguration , new ()
        {
            return  new TCOnfig().CreateMapper();
        }
    }
}
