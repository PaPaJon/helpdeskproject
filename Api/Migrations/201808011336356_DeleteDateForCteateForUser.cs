namespace HelpDesk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteDateForCteateForUser : DbMigration
    {
        public override void Up()
        {
            DropColumn("sec.users", "date_for_creates_Account");
        }
        
        public override void Down()
        {
            AddColumn("sec.users", "date_for_creates_Account", c => c.DateTime(nullable: false));
        }
    }
}
