namespace HelpDesk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentForUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.comments", "User_Id", c => c.Guid());
            CreateIndex("dbo.comments", "User_Id");
            AddForeignKey("dbo.comments", "User_Id", "sec.users", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.comments", "User_Id", "sec.users");
            DropIndex("dbo.comments", new[] { "User_Id" });
            DropColumn("dbo.comments", "User_Id");
        }
    }
}
