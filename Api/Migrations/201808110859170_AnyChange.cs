namespace HelpDesk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnyChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.comments", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.tickets", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("sec.users", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("sec.roles", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.tickets", "Description2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tickets", "Description2", c => c.String());
            DropColumn("sec.roles", "IsDeleted");
            DropColumn("sec.users", "IsDeleted");
            DropColumn("dbo.tickets", "IsDeleted");
            DropColumn("dbo.comments", "IsDeleted");
        }
    }
}
