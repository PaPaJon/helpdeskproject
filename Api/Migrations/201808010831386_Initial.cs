namespace HelpDesk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.comments",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        ticket_id = c.Guid(nullable: false),
                        text = c.String(),
                        create_date = c.DateTime(nullable: false),
                        modify_date = c.DateTime(nullable: false),
                        created_by_id = c.Guid(nullable: false),
                        modified_by_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tickets", t => t.ticket_id, cascadeDelete: true)
                .Index(t => t.ticket_id);
            
            CreateTable(
                "dbo.tickets",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        Header = c.String(),
                        Description = c.String(),
                        Description2 = c.String(),
                        status = c.Int(nullable: false),
                        priority = c.Int(nullable: false),
                        classifier = c.Int(nullable: false),
                        dead_line = c.DateTime(),
                        create_date = c.DateTime(nullable: false),
                        modify_date = c.DateTime(nullable: false),
                        created_by_id = c.Guid(nullable: false),
                        modified_by_id = c.Guid(nullable: false),
                        author_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("sec.users", t => t.author_id, cascadeDelete: true)
                .Index(t => t.author_id);
            
            CreateTable(
                "sec.users",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        user_name = c.String(),
                        login = c.String(),
                        pass = c.String(),
                        avatar = c.Binary(),
                        date_for_creates_Account = c.DateTime(nullable: false),
                        date_last_activity = c.DateTime(nullable: false),
                        create_date = c.DateTime(nullable: false),
                        modify_date = c.DateTime(nullable: false),
                        created_by_id = c.Guid(nullable: false),
                        modified_by_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "sec.roles",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        create_date = c.DateTime(nullable: false),
                        modify_date = c.DateTime(nullable: false),
                        created_by_id = c.Guid(nullable: false),
                        modified_by_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "sec.user_role_relation",
                c => new
                    {
                        user_id = c.Guid(nullable: false),
                        role_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.user_id, t.role_id })
                .ForeignKey("sec.roles", t => t.role_id, cascadeDelete: true)
                .ForeignKey("sec.users", t => t.user_id, cascadeDelete: true)
                .Index(t => t.user_id)
                .Index(t => t.role_id);
            
            CreateTable(
                "dbo.co_executor_ticket_relation",
                c => new
                    {
                        ticket_id = c.Guid(nullable: false),
                        co_executor_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ticket_id, t.co_executor_id })
                .ForeignKey("dbo.tickets", t => t.ticket_id, cascadeDelete: true)
                .ForeignKey("sec.users", t => t.co_executor_id, cascadeDelete: true)
                .Index(t => t.ticket_id)
                .Index(t => t.co_executor_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("sec.user_role_relation", "user_id", "sec.users");
            DropForeignKey("sec.user_role_relation", "role_id", "sec.roles");
            DropForeignKey("dbo.comments", "ticket_id", "dbo.tickets");
            DropForeignKey("dbo.co_executor_ticket_relation", "co_executor_id", "sec.users");
            DropForeignKey("dbo.co_executor_ticket_relation", "ticket_id", "dbo.tickets");
            DropForeignKey("dbo.tickets", "author_id", "sec.users");
            DropIndex("dbo.co_executor_ticket_relation", new[] { "co_executor_id" });
            DropIndex("dbo.co_executor_ticket_relation", new[] { "ticket_id" });
            DropIndex("sec.user_role_relation", new[] { "role_id" });
            DropIndex("sec.user_role_relation", new[] { "user_id" });
            DropIndex("dbo.tickets", new[] { "author_id" });
            DropIndex("dbo.comments", new[] { "ticket_id" });
            DropTable("dbo.co_executor_ticket_relation");
            DropTable("sec.user_role_relation");
            DropTable("sec.roles");
            DropTable("sec.users");
            DropTable("dbo.tickets");
            DropTable("dbo.comments");
        }
    }
}
