﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.EfConfigurations
{
    class UserConfiguration :EntityConfigurationBase<User>
    {
        public UserConfiguration()
        {
            Property(x => x.Avatar).HasColumnName("avatar");
            Property(x => x.DateLastActivity).HasColumnName("date_last_activity");
            Property(x => x.Login).HasColumnName("login");
            Property(x => x.Pass).HasColumnName("pass");
            Property(x => x.UserName).HasColumnName("user_name");
            Ignore(x => x.FIO);
            HasMany(x => x.Tickets).WithMany(y => y.CoExecutors);
                

            ToTable("users","sec");
        }
    }
}
