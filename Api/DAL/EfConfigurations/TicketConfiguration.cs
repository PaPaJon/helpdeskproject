﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.EfConfigurations
{
    class TicketConfiguration : EntityConfigurationBase<Ticket>
    {
        public TicketConfiguration()
        {
            Property(x => x.Classifier).HasColumnName("classifier");
            Property(x => x.Priority).HasColumnName("priority");
            Property(x => x.Status).HasColumnName("status");
            Property(x => x.DeadLine).HasColumnName("dead_line").IsOptional();
            Property(x => x.AuthorId).HasColumnName("author_id");
            HasRequired(x => x.Author).WithMany().HasForeignKey(x =>x.AuthorId);

            HasMany(x => x.CoExecutors).WithMany(y => y.Tickets).
                Map(x=> x.MapLeftKey("ticket_id").
                        MapRightKey("co_executor_id").
                        ToTable("co_executor_ticket_relation"));

            HasMany(x => x.Comments).WithRequired(x=> x.Ticket).HasForeignKey(c=>c.TicketId);
            

            ToTable("tickets");
        }
    }
}
