﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.EfConfigurations
{
    /// <summary>
    /// http://www.entityframeworktutorial.net/efcore/configure-many-to-many-relationship-in-ef-core.aspx
    /// </summary>
    class UserRoleRelationConfiguration : EntityTypeConfiguration<UserRoleRel>
    {
        public UserRoleRelationConfiguration()
        {
            Property(x => x.RoleId).HasColumnName("role_id").IsRequired();
            Property(x => x.UserId).HasColumnName("user_id").IsRequired();
            HasRequired(x => x.Role).WithMany().HasForeignKey(y => y.RoleId);
            HasRequired(x => x.User).WithMany().HasForeignKey(y => y.UserId);

            HasKey(x => new { x.UserId, x.RoleId } );

            ToTable("user_role_relation","sec");
        }
    }
}
