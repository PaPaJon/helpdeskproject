﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.EfConfigurations
{
    class RoleConfiguration : EntityConfigurationBase<Role>
    {
        public RoleConfiguration()
        {
            Property(x => x.Name).HasColumnName("name");

            ToTable("roles","sec");
        }
    }
}
