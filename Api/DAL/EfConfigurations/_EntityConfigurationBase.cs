﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.EfConfigurations
{
    abstract class EntityConfigurationBase<TEntity> : EntityTypeConfiguration<TEntity>
    where TEntity : EntityBase
    {
        protected EntityConfigurationBase()
        {
            Property(x => x.Id).HasColumnName("id");
            Property(x => x.CreateDate).HasColumnName("create_date");
            Property(x => x.CreatedById).HasColumnName("created_by_id");
            Property(x => x.ModifiedById).HasColumnName("modified_by_id");
            Property(x => x.ModifyDate).HasColumnName("modify_date");

        }
    }
}
