﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.EfConfigurations
{
    class CommentConfiguration : EntityConfigurationBase<Comment>
    {
        public CommentConfiguration()
        {
            Property(x => x.Text).HasColumnName("text");
            Property(x => x.TicketId).HasColumnName("ticket_id");
            HasRequired(x => x.Ticket).WithMany(y => y.Comments).HasForeignKey(x=> x.TicketId);

            ToTable("comments");
        }
    }
}
