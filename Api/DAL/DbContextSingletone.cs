﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.DAL
{
    static class DbContextSingletone
    {
        private static HelpDeskDbContext _context;

        public static HelpDeskDbContext GetDbContext()
        {
            if (_context == null)
            {
                _context = new HelpDeskDbContext();
            }
            return _context;
        }
    }
}
