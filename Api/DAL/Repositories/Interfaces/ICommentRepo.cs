﻿using HelpDesk.Api.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.DAL.Repositories.Interfaces
{
    public interface ICommentRepo : IRepository<Comment, Guid>
    {

    }
}
