﻿using System;
using System.Linq;
using HelpDesk.Api.Entity;
using HelpDesk.Api.Filters;

namespace HelpDesk.DAL.Repositories.Interfaces
{
    public interface IRepository<TEntity, Tkey> 
        where TEntity : EntityBase
    {
        TEntity Create(TEntity entity);
        //[Obsolete("Вместо этого метода используте метод NewUpdate()")]
        TEntity Update(TEntity entity);
        void Delete(Guid id, bool isPermanently = false);
        IQueryable<TEntity> GetAll(bool deleted = false);
        TEntity Get(Guid id);
    }
}