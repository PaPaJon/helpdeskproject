﻿using System;
using HelpDesk.Api.Entity;

namespace HelpDesk.DAL.Repositories.Interfaces
{
    public interface ITicketRepo : IRepository<Ticket,Guid>
    {
    }
}
