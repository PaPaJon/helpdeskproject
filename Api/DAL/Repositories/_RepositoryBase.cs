﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
//using Api.Entity;
using Api.Utility;
using HelpDesk.Api.Entity;
using HelpDesk.DAL.Repositories.Interfaces;

namespace HelpDesk.DAL.Repositories
{
    public abstract class RepositoryBase<TEntity, Tkey> : IRepository<TEntity,Tkey>
        where TEntity : EntityBase
    {
        private HelpDeskDbContext _context = DbContextSingletone.GetDbContext();

        public virtual TEntity Create(TEntity entity)
        {
            entity.Id = GuidGenerator.GetGuid();
            entity.CreateDate = DateTime.Now;
            entity.ModifyDate = entity.CreateDate;
            entity.CreatedById = GuidGenerator.GetGuid();
            entity.ModifiedById = GuidGenerator.GetGuid();

            _context.GetSet<TEntity>().Add(entity);

            SaveChanges();
            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            entity.ModifiedById = Guid.NewGuid();
            entity.ModifyDate = DateTime.Now;

            SaveChanges();

            return entity;
        }

        public virtual void Delete(Guid id,  bool isPermanently = false)
        {
            try
            {
                var entity = _context.GetSet<TEntity>().Find(id);

                if (isPermanently)
                {
                    _context.GetSet<TEntity>().Remove(entity);
                }
                else
                {
                    entity.IsDeleted = true;
                    entity.ModifyDate = DateTime.Now;
                }

                SaveChanges();
            }
            catch (Exception e)
            {
                switch (e.GetType().Name)
                {
                    case "NotSupportedException" :
                        throw new Exception("Неподерживаемая в данный момент операция", e);

                    case
                        "AccessViolationException": throw new Exception("Невозможно получить доступ к источнику данных", e);
                }
            }
        }

        public virtual IQueryable<TEntity> GetAll(bool deleted = false)
        {
            return _context.GetSet<TEntity>().Where(x => x.IsDeleted == deleted);
        }

        public virtual TEntity Get(Guid id)
        {
            return _context.GetSet<TEntity>().FirstOrDefault(x => x.Id == id);
        }

        private void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
