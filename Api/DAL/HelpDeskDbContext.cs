﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api.Entity;
using HelpDesk.DAL.EfConfigurations;

namespace HelpDesk.DAL
{

    class HelpDeskDbContext : DbContext
    {
        //public DbSet<Comment> Comments { get; set; }
        //public DbSet<Role> Roles { get; set; }
        //public DbSet<Ticket> Tickets { get; set; }
        //public DbSet<User> Users { get; set; }
        //public DbSet<UserRoleRel> UserRoleRels { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            //modelBuilder.Ignore<User>();
            
            modelBuilder.Configurations.Add(new CommentConfiguration());
            modelBuilder.Configurations.Add(new TicketConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new UserRoleRelationConfiguration());


        }

        public DbSet<TEntity> GetSet<TEntity>()
        where TEntity : class
        {
            return Set<TEntity>();
        }
        
    }
}
