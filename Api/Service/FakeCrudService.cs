﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Api;
using HelpDesk.Api.Filters;
using HelpDesk.DataContract;

namespace HelpDesk.Service
{
    public class FakeCrudService<TDto> : ICrudService<TDto>
    where TDto : DtoBase, new()
    {
        public TDto Create(TDto dto)
        {
            if (dto != null)
            {
                dto.Id = Guid.NewGuid();
                dto.CreateDate = DateTime.Now;
                dto.ModifyDate = dto.CreateDate;
            }

            return dto;
        }

        public bool Delete(Guid id)
        {
            return true;
        }

        public TDto Read(Guid id)
        {
            return new TDto(){Id = id};
        }

        public IList<TDto> Read(FilterBase filter)
        {
            return new [] {new TDto(){Id = Guid.NewGuid()}, new TDto(){Id = Guid.NewGuid()}};
        }

        public TDto Update(TDto dto)
        {
            dto.ModifyDate = DateTime.Now;
            return dto;
        }
    }
}
