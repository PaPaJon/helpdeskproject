﻿using HelpDesk.Api.Entity;
using AutoMapper;
using HelpDesk.DataContract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using HelpDesk.Api;
using HelpDesk.Api.Filters;
using HelpDesk.AutoMapper;
using HelpDesk.DAL.Repositories;
using HelpDesk.DAL.Repositories.Interfaces;
using HelpDesk.Migrations;

namespace HelpDesk.Service
{
    public interface ITicketCrudService : ICrudService<TicketDto> { }
    public class TicketCrudService : ITicketCrudService
    {
        private IRepository<Ticket, Guid> _repo;
        private IMapper _mapper = AutoMapperBuilder.GetMapper<AutoMapperConfiguration>();

        public TicketCrudService(IRepository<Ticket, Guid> repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }

        public TicketDto Create(TicketDto dto)
        {
            if (dto != null)
            {
                Validate(dto);

                var ticekt = _mapper.Map<TicketDto, Ticket>(dto);

                ticekt = _repo.Create(ticekt);

                var author = ticekt.Author;

                return _mapper.Map<Ticket, TicketDto>(ticekt);
            }

            return null;
        }

        public bool Delete(Guid id)
        {
            // произвести проверку на возможность удаления
            try
            {
                _repo.Delete(id);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true ;
        }

        public TicketDto Read(Guid id)
        {
            return _mapper.Map<Ticket,TicketDto>(_repo.Get(id));
        }

        public IList<TicketDto> Read(FilterBase filter)
        {
            var query = _repo.GetAll();
            // использовать LINQ для преобразования списка сушьностей в списко DTO
            var _filter = (TicketFilter) filter;
            if (!string.IsNullOrEmpty(_filter.DescriptionSubString))
            {
                query = query.Where(x => x.Description.Contains(_filter.DescriptionSubString));
            }

            if (!string.IsNullOrEmpty(_filter.HeaderSubString))
            {
                query = query.Where(x => x.Header.Contains(_filter.HeaderSubString));
            }

            var list = query.ToList();

            var listDto = list.Select(x => _mapper.Map<Ticket, TicketDto>(x)).ToList();

            return listDto;
            
        }

        public TicketDto Update(TicketDto dto)
        {
            if (dto != null)
            {
                Validate(dto);

                var ticket = _repo.Update(_mapper.Map<TicketDto, Ticket>(dto));
                return _mapper.Map<Ticket, TicketDto>(ticket);
            }

            return null;
        }

        public void Validate(TicketDto dto)
        {
            string message = String.Empty;

            if (dto.AuthorId == Guid.Empty)
            {
                message = "автор не указан";
            }

            if (string.IsNullOrEmpty(dto.Header))
            {
                message += "заголовок не указан";
            }

            if (!string.IsNullOrEmpty(message))
            {
                throw new ArgumentException(message);
            }
        }
    }
}