﻿using HelpDesk.Api;
using HelpDesk.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HelpDesk.Api.Filters;

namespace HelpDesk.WebApiv2.Controllers
{
    public class UserController
    {
        private ICrudService<UserDto> _service;

        //public TicketController(ICrudService<TicketDto> service)
        //{
        //    _service = service ?? throw new ArgumentNullException(nameof(service));
        //}
        [HttpGet]
        public IList<UserDto> Get(UserDto userDto)
        {
            // var tickets = new[] { new TicketDto { Id = Guid.NewGuid() }, new TicketDto { Id = Guid.NewGuid() } } ;
            //TicketService.GetTickets();
            return _service.Read(new UserFiler());
        }

        [HttpGet]
        public UserDto Get(Guid id)
        {
            return _service.Read(id);
        }

        [HttpPost]
        public UserDto Create(String header, string description, Guid authorId)
        {
            if (header == null && authorId == Guid.Empty)
            {

            }
            //var ticket = _service.Create(new TicketDto() { Header = header, Description = description, AuthorId = authorId });
            var user = new UserDto() { };
            user.Id = Guid.NewGuid();
            return user;
        }

        [HttpPut]
        public UserDto Update(Guid id, string header, string description)
        {
            UserDto user = new UserDto() { Id = Guid.NewGuid() };
            //var q = _service.Read(new TicketFilter()).Where(x =>x.Id==id);


            //if (id == _service.Read(new TicketFilter() { Id = id }))
            //{
            //    ticket = new TicketDto() { Id = id, Header = header, Description = description, AuthorId = authorId };
            //}

            return user;
        }

        [HttpDelete]
        public bool Delete(Guid id)
        {
            return _service.Delete(id);
        }
    }
}