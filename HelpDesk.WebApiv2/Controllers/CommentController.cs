﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HelpDesk.Api;
using HelpDesk.Api.Entity;
using HelpDesk.Api.Filters;
using HelpDesk.DataContract;

namespace HelpDesk.WebApiv2.Controllers
{
    public class CommentController : ApiController
    {
        private ICrudService<CommentDto> _service;

        //public TicketController(ICrudService<TicketDto> service)
        //{
        //    _service = service ?? throw new ArgumentNullException(nameof(service));
        //}
        [HttpGet]
        public IList<CommentDto> Get(CommentDto commentDto)
        {
            // var tickets = new[] { new TicketDto { Id = Guid.NewGuid() }, new TicketDto { Id = Guid.NewGuid() } } ;
            //TicketService.GetTickets();
            return _service.Read(new CommentFilter(commentDto));
        }

        [HttpGet]
        public CommentDto Get(Guid id)
        {
            return _service.Read(id);
        }

        [HttpPost]
        public CommentDto Create(String header, string description, Guid authorId)
        {
            if (header == null && authorId == Guid.Empty)
            {

            }
            //var ticket = _service.Create(new TicketDto() { Header = header, Description = description, AuthorId = authorId });
            var comment = new CommentDto() {  };
            comment.Id = Guid.NewGuid();
            return comment;
        }

        [HttpPut]
        public CommentDto Update(Guid id, string header, string description)
        {
            CommentDto comment = new CommentDto() { Id = Guid.NewGuid() };
            //var q = _service.Read(new TicketFilter()).Where(x =>x.Id==id);


            //if (id == _service.Read(new TicketFilter() { Id = id }))
            //{
            //    ticket = new TicketDto() { Id = id, Header = header, Description = description, AuthorId = authorId };
            //}

            return comment;
        }

        [HttpDelete]
        public bool Delete(Guid id)
        {
            return _service.Delete(id);
        }
    }
}