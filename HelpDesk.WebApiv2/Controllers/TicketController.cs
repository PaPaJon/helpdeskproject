﻿using HelpDesk.Api;
using HelpDesk.Api.Filters;
using HelpDesk.DataContract;
using HelpDesk.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HelpDesk.DAL.Repositories;

namespace HelpDesk.WebApiv2.Controllers
{
    public class TicketController : ApiController
    {
        public ICrudService<TicketDto> Service { get; set; } = new TicketCrudService(new TicketRepo());

        [HttpGet]
        public IList<TicketDto> Get(TicketDto ticketDto)
        {
            return Service.Read(new TicketFilter());
        }

        [HttpGet]
        public TicketDto Get (Guid? id)
        {
            if (id==null || id == Guid.Empty  )
            {
                return null;
            }

            return Service.Read((Guid)id);
        }

        [HttpPost]
        public TicketDto Create (string header, string description, Guid authorId)
        {
            if (string.IsNullOrEmpty(header) || authorId == Guid.Empty)
            {
                return null;
            }

            var ticket = new TicketDto() {Header = header, Description = description, AuthorId = authorId};
            try
            {
               ticket = Service.Create(ticket);

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }



            return ticket;
        }

        [HttpPut]
        public TicketDto Update (Guid id, string header, string description, Guid authorId)
        {
            var ticket = Service.Update(new TicketDto()
            {
                Id = id,
                Header = header,
                Description = description,
                AuthorId = authorId
            });

            return ticket ;
        }

        [HttpDelete]
        public bool Delete(Guid id)
        {
            return Service.Delete(id);
        }
    }
}
