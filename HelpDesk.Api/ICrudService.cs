﻿
using HelpDesk.Api.Filters;
using HelpDesk.DataContract;
using System;
using System.Collections.Generic;

namespace HelpDesk.Api
{
    public interface ICrudService<TDto>
        where TDto : DtoBase
    {
        TDto Create(TDto dto);
        TDto Read(Guid id);
        IList<TDto> Read(FilterBase filter);
        TDto Update(TDto dto);
        bool Delete(Guid id);
    }
}
