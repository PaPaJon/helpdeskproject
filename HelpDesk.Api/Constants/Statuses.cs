﻿namespace HelpDesk.Api.Constants
{
    public enum Statuses
    {
        Create = 0,
        Open = 10,
        InProgress = 60,
        Wait = 70,
        Done = 90,
        TaskClose = 100,
        TaskRemoved = 101
    }
}
