﻿namespace HelpDesk.Api.Constants
{
    public enum Priorities
    {
        Critical,
        Urgent,
        Planned,
        Secondary
    }
}
