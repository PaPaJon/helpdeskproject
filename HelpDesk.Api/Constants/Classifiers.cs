﻿namespace HelpDesk.Api.Constants
{
    public enum Classifiers
    {
        Incident,
        Breaking,
        Consultation
    }
}
