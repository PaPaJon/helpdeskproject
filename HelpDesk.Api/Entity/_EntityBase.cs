﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Api.Entity
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid CreatedById { get; set; }
        public Guid ModifiedById { get; set; }
        public bool IsDeleted{ get; set; }
    }
}
