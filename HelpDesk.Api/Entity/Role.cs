﻿using System.Collections.Generic;
using HelpDesk.Api.Entity;

namespace HelpDesk.Api.Entity
{
    public class Role : EntityBase
    {
        public string Name { get; set; }

    }
}