﻿using System;
using System.Dynamic;

namespace HelpDesk.Api.Entity
{
    public class Comment : EntityBase
    {
        public Guid? TicketId { get; set; }
        public Ticket Ticket { get; set; }
        public string Text { get; set; }
    }
}
