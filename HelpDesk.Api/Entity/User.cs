﻿using System;
using System.Collections.Generic;


namespace HelpDesk.Api.Entity
{
    public class User : EntityBase
    {
        public string UserName { get; set; }
        public string FIO { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public byte[] Avatar { get; set; }
        public IList<Ticket> Tickets { get; set; }
        public IList<Comment> Comment { get; set; }
        public DateTime DateLastActivity { get; set; }
       
    }
}
