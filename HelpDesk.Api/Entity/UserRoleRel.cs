﻿using System;
using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Api.Entity
{
    public class UserRoleRel
    {
        public Guid RoleId { get; set; }

        public virtual  Role Role { get; set; }
        
        public Guid UserId { get; set; }

        public virtual User User { get; set; }
    }
}
