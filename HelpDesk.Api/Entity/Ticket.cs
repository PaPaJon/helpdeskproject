﻿using HelpDesk.Api.Constants;
using System;
using System.Collections.Generic;


namespace HelpDesk.Api.Entity
{
    public class Ticket : EntityBase
    {
        public string Header { get; set; }
        public string Description { get; set; }
        public Guid AuthorId { get; set; }
        public virtual User Author { get; set; } 
        public virtual IList<User> CoExecutors { get; set; }
        public Statuses Status { get; set; }
        public Priorities Priority { get; set; }
        public Classifiers Classifier { get; set; }
        public virtual IList<Comment> Comments { get; set; }
        public DateTime? DeadLine { get; set; }
    }
}
