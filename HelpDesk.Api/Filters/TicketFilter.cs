﻿using HelpDesk.Api.Constants;
using HelpDesk.Api.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.DataContract;

namespace HelpDesk.Api.Filters
{
    public class TicketFilter : FilterBase
    {
        public string HeaderSubString { get; set; }
        public string DescriptionSubString { get; set; }
        //public virtual User Author { get; set; }
    }
}
